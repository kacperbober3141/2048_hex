# Wstawiona scena na grę w okienko
# podstawka pod grę
import math
import sys
from PySide6 import QtCore, QtWidgets, QtGui
import constants as c
import random


# for key, value in CELL_COLOR_DICT.items():
#     h = value.lstrip('#')
#     CELL_FONT_COLOR[key] = tuple(int(h[i:i+2], 16)/256 for i in (0, 2, 4))

# h = BACKGROUND_COLOR_CELL_EMPTY.lstrip('#')
# BACKGROUND_COLOR_CELL_EMPTY = tuple(int(h[i:i+2], 16)/256 for i in (0, 2, 4))
# print(BACKGROUND_COLOR_CELL_EMPTY)


class Hex(QtWidgets.QGraphicsItem):
    def __init__(self, scene, q_cord, r_cord, initial_value, scene_size, grid_size):
        super().__init__()
        self.value = initial_value
        self.scene = scene

        self.size = scene_size / ((grid_size * 2 + 2) * 2)
        self.width = self.size * math.sqrt(3)
        self.height = self.size * 2

        self.center = 0
        self.q = q_cord
        self.r = r_cord

        self.x_pos = 0
        self.y_pos = 0

        self.background_brush = QtGui.QBrush(
            QtGui.QColor.fromRgbF(c.BACKGROUND_COLOR_CELL_EMPTY[0], c.BACKGROUND_COLOR_CELL_EMPTY[1],
                                  c.BACKGROUND_COLOR_CELL_EMPTY[2]))
        self.text_color = QtGui.QColor.fromRgbF(c.BACKGROUND_COLOR_CELL_EMPTY[0], c.BACKGROUND_COLOR_CELL_EMPTY[1],
                                                c.BACKGROUND_COLOR_CELL_EMPTY[2])

        # pen
        self.hex_pen = QtGui.QPen()
        self.hex_pen.setBrush(self.background_brush)
        self.hex_pen.setWidth(2)

        # hex_polygon
        self.hexagon = self.create_polygon()
        self.polygon_item = self.scene.addPolygon(self.hexagon)
        self.polygon_item.setBrush(self.background_brush)
        self.polygon_item.setPen(self.hex_pen)
        self.offset_to_cords()
        self.polygon_item.setPos(self.center + self.x_pos, self.center + self.y_pos)

        # text inside polygon
        self.text_inside_polygon = QtWidgets.QGraphicsTextItem(str(self.value), self.polygon_item)
        self.text_inside_polygon.setDefaultTextColor(self.text_color)
        self.text_inside_polygon.setFont(
            QtGui.QFont("Clear Sans", int(self.size / (len(str(self.value)) * 0.35 + 1.2)), QtGui.QFont.Bold))
        # align co center
        rect = self.text_inside_polygon.boundingRect()
        rect.moveCenter(self.boundingRect().center())
        self.text_inside_polygon.setPos(rect.topLeft())

        self.color_hexagon()

    def boundingRect(self):
        return QtCore.QRectF(self.x_pos - self.size, self.y_pos - self.size, self.size * 2, self.size * 2)

    def paint(self, painter, option):
        painter.setPen(self.hex_pen)

        # Hexagon drawing
        if self.value == 0:
            color = QtGui.QColor.fromRgbF(c.BACKGROUND_COLOR_CELL_EMPTY[0], c.BACKGROUND_COLOR_CELL_EMPTY[1],
                                          c.BACKGROUND_COLOR_CELL_EMPTY[2])
            self.background_brush.setColor(color)
            painter.setBrush(self.background_brush)
        else:
            color = QtGui.QColor.fromRgbF(c.CELLS_COLOR[self.value][0], c.CELLS_COLOR[self.value][1],
                                          c.CELLS_COLOR[self.value][2])
            self.background_brush.setColor(color)
            painter.setBrush(self.background_brush)



    def set_value(self, value):
        self.value = value

    def create_polygon(self):
        triangle = QtGui.QPolygonF()
        for i in range(0, 6):
            angle_deg = 60 * i - 30
            angle_rad = math.pi / 180 * angle_deg
            x = self.center + self.size * math.cos(angle_rad)
            y = self.center + self.size * math.sin(angle_rad)
            triangle.append(QtCore.QPointF(x, y))

        return triangle

    def axial_to_cube(self):
        x = self.q
        z = self.r
        y = x - z
        return x, y, z

    def cube_to_offset(self):
        x, y, z = self.axial_to_cube()
        col = x + (z + (z & 1)) / 2
        row = z
        return col, row

    def offset_to_cords(self):
        col, row = self.cube_to_offset()

        if row & 1 == 0:  # if row is even 3/4 h, 1 w
            self.x_pos = self.width / 2 + col * self.width
            self.y_pos = self.height * row * 3 / 4
        else:
            self.x_pos = col * self.width
            self.y_pos = self.height * row * 3 / 4

        self.x_pos *= 1.15
        self.y_pos *= 1.15

    def refresh_hex(self):
        self.color_hexagon()
        self.polygon_item.setBrush(self.background_brush)

        self.text_inside_polygon.setPlainText(str(self.value))
        self.text_inside_polygon.setFont(
            QtGui.QFont("Clear Sans", int(self.size / (len(str(self.value)) * 0.35 + 1.2)), QtGui.QFont.Bold))
        rect = self.text_inside_polygon.boundingRect()
        rect.moveCenter(self.boundingRect().center())
        self.text_inside_polygon.setPos(rect.topLeft())
        self.text_inside_polygon.setDefaultTextColor(self.text_color)

        self.hex_pen.setBrush(self.background_brush)
        self.polygon_item.setPen(self.hex_pen)

    def color_hexagon(self):
        if self.value == 0:
            color = QtGui.QColor.fromRgbF(c.BACKGROUND_COLOR_CELL_EMPTY[0], c.BACKGROUND_COLOR_CELL_EMPTY[1],
                                          c.BACKGROUND_COLOR_CELL_EMPTY[2])
        else:
            color = QtGui.QColor.fromRgbF(c.CELLS_COLOR[self.value][0],
                                          c.CELLS_COLOR[self.value][1],
                                          c.CELLS_COLOR[self.value][2])
        self.background_brush.setColor(color)

        if self.value == 0:
            self.text_color = QtGui.QColor.fromRgbF(c.BACKGROUND_COLOR_CELL_EMPTY[0],
                                                    c.BACKGROUND_COLOR_CELL_EMPTY[1],
                                                    c.BACKGROUND_COLOR_CELL_EMPTY[2])
        else:
            self.text_color = QtGui.QColor.fromRgbF(c.CELL_FONT_COLOR[self.value][0],
                                                    c.CELL_FONT_COLOR[self.value][1],
                                                    c.CELL_FONT_COLOR[self.value][2])


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Gra 2048 heksagonalnie")
        self.setGeometry(300, 300, 1000, 800)

        self.scene = QtWidgets.QGraphicsScene(self)
        self.view = QtWidgets.QGraphicsView(self.scene, self)
        self.view.setRenderHint(QtGui.QPainter.Antialiasing)
        self.view.setViewportUpdateMode(QtWidgets.QGraphicsView.FullViewportUpdate)
        self.view.setGeometry(30, 30, 600, 600)

        self.grid_size = 3
        self.hex_items = []
        self.initialize_hex_grid()
        self.initialize_game()
        for hexagon in self.hex_items:
            hexagon.refresh_hex()

        # self.scene.update()

    def initialize_hex_grid(self):

        grid_size = self.grid_size
        scene_size = self.view.width()
        for x in range(-grid_size, grid_size + 1):
            for y in range(-grid_size, grid_size + 1):
                for z in range(-grid_size, grid_size + 1):
                    if x + y + z == 0:
                        self.hex_items.append(Hex(self.scene, x, z, 0, scene_size, grid_size))

    def initialize_game(self):
        for i in range(self.grid_size):
            random_index = random.randint(0, len(self.hex_items) - 1)
            self.hex_items[random_index].set_value(2)


if __name__ == "__main__":
    app = QtWidgets.QApplication([])
    widget = MainWindow()
    widget.show()

    sys.exit(app.exec_())
